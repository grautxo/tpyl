from distutils.dir_util import mkpath
import os
import shutil
import stat
import subprocess
import tempfile

from nose.plugins.attrib import attr
from six import StringIO
from unittest import TestCase
import requests_mock

import tpyl


TMP = tempfile.gettempdir()
DIR_TESTS = os.path.join(TMP, 'tpyl-tests')

EXAMPLE_TEMPLATE = '''
This is a text
with variables like: {{ foo }}
and a loop
{% for x in bar -%}
  {{ x }}
{% endfor %}
'''

EXAMPLE_CONTEXT = {
    'foo': 'my-val',
    'bar': [1, 2, 3],
}

EXPECTED_RENDER = '''
This is a text
with variables like: my-val
and a loop
1
2
3
'''

CONTEXT_FILES = {
    'template.j2': EXAMPLE_TEMPLATE,
    'context.env': '''
my-env-string=hello world
my-env-void-var
''',
    'context.ini': '''
[DEFAULT]
my-ini-string = hello world

[a-section]
foo-section = bar

[b-section]
lol-section = juas!
lol-more = xD
''',
    'context.yaml': '''
---
my-yaml-string: hello world
my-yaml-list:
- one
- two
- three
my-yaml-dict:
  one: a
  two: b
  three: c
''',
    'context.json': '''
{
  "my-json-string": "hello world",
  "my-json-list": [
    "one",
    "two",
    "three"
  ],
  "my-json-dict": {
    "one": "a",
    "two": "b",
    "three": "c"
  }
}
''',
    'context.toml': '''
my-toml-string = "hello world"
my-toml-list = [
  "one",
  "two",
  "three"
]

[my-toml-dict]
one = "a"
two = "b"
three = "c"
''',
    'context.exec': '''
#!/usr/bin/env sh
echo "MY_EXEC_VAR=MY_EXEC_VALUE"
'''.lstrip(),
    'context.py': '''
#!/usr/bin/env python
yaml = """
---
my-py-string: my-py-value
my-py-dict:
  one: a
  two: b
  three: c
""".lstrip()
print(yaml)
'''.lstrip(),
    'cli-yaml.txt': '''
---
foo: my-val
bar:
- 1
- 2
- 3
''',
    'cli-json.txt': '''
{
  "foo": "my-val",
  "bar": [1, 2, 3]
}
''',
    'cli-partial-yaml.txt': '''
---
foo: my-val
''',
    'cli-partial-json.txt': '''
{
  "bar": [1, 2, 3]
}
''',
    'emtpy-ini.txt': ''
}

FILTERS_NAME = 'filters.py'
FILTERS_EXAMPLE = '''
NOT_CALLABLE = ':)'


def my_custom_filter(val, num=1, sep=':'):
    return sep.join([val for _ in range(0, num)])
'''
EXAMPLE_FILTERS_NAME = 'template-filters.j2'
EXAMPLE_TEMPLATE_FILTER = '''
my filtered templates
are awesome, look at this {{ foo | my_custom_filter(2) }}
'''
EXPECTED_TEMPLATE_FILTER = '''
my filtered templates
are awesome, look at this my-val:my-val
'''


TEMPLATE_URL = 'http://tpyl.com/template'
CONTEXT_URL = 'http://tpyl.com/context'
CONTEXT_URL_CONTENT = '''---
my-url-string: my-url-value
my-url-dict:
  one: a
  two: b
  three: c
'''


def setup():
    # Create test context files
    mkpath(DIR_TESTS)
    for file in CONTEXT_FILES:
        with open(_file(file), 'w') as f:
            f.write(CONTEXT_FILES[file])
        _, ext = os.path.splitext(file)
        with open(_file(ext.lstrip('.')), 'w') as f:
            f.write(CONTEXT_FILES[file])
    # Write the filter file
    with open(_file(EXAMPLE_FILTERS_NAME), 'w') as f:
        f.write(EXAMPLE_TEMPLATE_FILTER)
    with open(_file(FILTERS_NAME), 'w') as f:
        f.write(FILTERS_EXAMPLE)
    # Make executable contexts
    for f in [
        'context.exec',
        'context.py',
    ]:
        ex = _file(f)
        st = os.stat(ex)
        os.chmod(ex, st.st_mode | stat.S_IEXEC)


def teardown():
    shutil.rmtree(DIR_TESTS)


def _file(file):
    return os.path.join(DIR_TESTS, file.lstrip(os.sep))


class ContextTest(TestCase):

    def _expect_ctx(self, context=None, var=None, env=False, to_be=None):
        to_be = to_be or {}
        ctx = tpyl.load_context(var=var, context=context, env=env)
        self.assertEqual(ctx, to_be)

    def test_context_vars(self):
        self._expect_ctx(
            var=(
                'my-val=my-value',
                'foo=bar',
            ),
            to_be={
                'my-val': 'my-value',
                'foo': 'bar',
            })

    def test_context_env_var(self):
        ctx = tpyl.load_context(var=(
            'my-val=my-value',
            'foo=bar',
        ), env=True)
        self.assertIn('HOME', ctx)

    def test_context_yaml(self):
        self._expect_ctx(
            context=(
                _file('context.yaml'),
            ), to_be={
                'my-yaml-string': 'hello world',
                'my-yaml-list': ['one', 'two', 'three'],
                'my-yaml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_json(self):
        self._expect_ctx(
            context=(
                _file('context.json'),
            ), to_be={
                'my-json-string': 'hello world',
                'my-json-list': ['one', 'two', 'three'],
                'my-json-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_toml(self):
        self._expect_ctx(
            context=(
                _file('context.toml'),
            ), to_be={
                'my-toml-string': 'hello world',
                'my-toml-list': ['one', 'two', 'three'],
                'my-toml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_env(self):
        self._expect_ctx(
            context=(
                _file('context.env'),
            ), to_be={
                'my-env-string': 'hello world',
                'my-env-void-var': None,
            })

    def test_context_ini(self):
        ctx = tpyl.load_context(context=(_file('context.ini'),), env=False)
        self.assertIn('DEFAULT', ctx)
        self.assertEqual(ctx['DEFAULT']['my-ini-string'], 'hello world')
        self.assertIn('a-section', ctx)
        self.assertEqual(ctx['a-section']['foo-section'], 'bar')
        self.assertIn('b-section', ctx)
        self.assertEqual(ctx['b-section']['lol-section'], 'juas!')
        self.assertEqual(ctx['b-section']['lol-more'], 'xD')

    def test_context_ini_empty(self):
        ctx = tpyl.load_context(context=(_file('emtpy-ini.txt'),), env=False)
        self.assertEqual(ctx, {})

    def test_context_exec(self):
        self._expect_ctx(
            context=(
                _file('context.exec'),
            ), to_be={
                'MY_EXEC_VAR': 'MY_EXEC_VALUE',
            })

    def test_context_exec_py(self):
        self._expect_ctx(
            context=(
                _file('context.py'),
            ), to_be={
                'my-py-string': 'my-py-value',
                'my-py-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    @requests_mock.Mocker()
    def test_context_url(self, mocker):
        mocker.get(CONTEXT_URL, text=CONTEXT_URL_CONTENT)
        self._expect_ctx(
            context=(
                CONTEXT_URL,
            ), to_be={
                'my-url-string': 'my-url-value',
                'my-url-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_identify_yaml(self):
        self._expect_ctx(
            context=(
                _file('yaml'),
            ), to_be={
                'my-yaml-string': 'hello world',
                'my-yaml-list': ['one', 'two', 'three'],
                'my-yaml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_identify_json(self):
        self._expect_ctx(
            context=(
                _file('json'),
            ), to_be={
                'my-json-string': 'hello world',
                'my-json-list': ['one', 'two', 'three'],
                'my-json-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_identify_toml(self):
        self._expect_ctx(
            context=(
                _file('toml'),
            ), to_be={
                'my-toml-string': 'hello world',
                'my-toml-list': ['one', 'two', 'three'],
                'my-toml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
            })

    def test_context_identify_env(self):
        self._expect_ctx(
            context=(
                _file('env'),
            ), to_be={
                'my-env-string': 'hello world',
                'my-env-void-var': None,
            })

    def test_context_identify_ini(self):
        ctx = tpyl.load_context(context=(_file('ini'),), env=False)
        self.assertIn('DEFAULT', ctx)
        self.assertEqual(ctx['DEFAULT']['my-ini-string'], 'hello world')
        self.assertIn('a-section', ctx)
        self.assertEqual(ctx['a-section']['foo-section'], 'bar')
        self.assertIn('b-section', ctx)
        self.assertEqual(ctx['b-section']['lol-section'], 'juas!')
        self.assertEqual(ctx['b-section']['lol-more'], 'xD')

    def test_context_combined(self):
        self._expect_ctx(
            context=(
                _file('context.yaml'),
                _file('context.json'),
                _file('context.env'),
            ), to_be={
                'my-yaml-string': 'hello world',
                'my-yaml-list': ['one', 'two', 'three'],
                'my-yaml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
                'my-json-string': 'hello world',
                'my-json-list': ['one', 'two', 'three'],
                'my-json-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
                'my-env-string': 'hello world',
                'my-env-void-var': None,
            })

    def test_context_combined_and_vars(self):
        self._expect_ctx(
            var=(
                'foo=bar',
                'bar=foo',
            ),
            context=(
                _file('context.yaml'),
                _file('context.json'),
                _file('context.env'),
            ), to_be={
                'foo': 'bar',
                'bar': 'foo',
                'my-yaml-string': 'hello world',
                'my-yaml-string': 'hello world',
                'my-yaml-list': ['one', 'two', 'three'],
                'my-yaml-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
                'my-json-string': 'hello world',
                'my-json-list': ['one', 'two', 'three'],
                'my-json-dict': {'one': 'a', 'two': 'b', 'three': 'c'},
                'my-env-string': 'hello world',
                'my-env-void-var': None,
            })


class RenderingTest(TestCase):

    def _expect_render(self, file, to_be=None, filters=None, stream=None):
        to_be = to_be or ''
        render = tpyl.render_template(file, EXAMPLE_CONTEXT,
                                      filters=filters,
                                      stream=stream)
        self.assertEqual(render, to_be)

    def test_render(self):
        self._expect_render(_file('template.j2'), to_be=EXPECTED_RENDER)

    def test_render_stream(self):
        stream = StringIO(EXAMPLE_TEMPLATE)
        self._expect_render('-', EXPECTED_RENDER, stream=stream)

    def test_render_file_path(self):
        path = _file('test-render')
        tpyl.write_render(EXPECTED_RENDER, path)
        self.assertTrue(os.path.exists(path))
        self._expect_render(path, to_be=EXPECTED_RENDER.rstrip())
        os.remove(path)

    def test_render_file_stream(self):
        stream = StringIO()
        tpyl.write_render(EXPECTED_RENDER, stream=stream)
        stream.seek(0)
        self.assertEqual(EXPECTED_RENDER, stream.read())

    @requests_mock.Mocker()
    def test_render_url(self, mocker):
        mocker.get(TEMPLATE_URL, text=EXAMPLE_TEMPLATE)
        self._expect_render(TEMPLATE_URL, to_be=EXPECTED_RENDER)

    def test_custom_filters(self):
        file_path = _file(FILTERS_NAME)
        # Write a pyc file to test pyclean functionallity on module import
        pyc = '%sc' % _file(FILTERS_NAME)
        with open(pyc, 'w') as f:
            f.write('')
        self._expect_render(_file(EXAMPLE_FILTERS_NAME),
                            filters=(file_path),
                            to_be=EXPECTED_TEMPLATE_FILTER.rstrip())


class CustomFiltersTest(TestCase):

    def test_is_truthy(self):
        fn = tpyl.filters.is_truthy
        for value in [True, 'True', 'true', 'TRUE', 'YES', 'yes', '1']:
            self.assertTrue(fn(value))
        for value in [False, 'False', 'false', 'FALSE', 'NO', 'no', '0']:
            self.assertFalse(fn(value))
        self.assertTrue(fn(None, default=True))
        self.assertFalse(fn(None, default=False))

    def test_is_falsy(self):
        fn = tpyl.filters.is_falsy
        for value in [False, 'False', 'false', 'FALSE', 'NO', 'no', '0']:
            self.assertTrue(fn(value))
        for value in [True, 'True', 'true', 'TRUE', 'YES', 'yes', '1']:
            self.assertFalse(fn(value))
        self.assertFalse(fn(None, default=True))
        self.assertTrue(fn(None, default=False))


def sh(command):
    return subprocess.check_output(command, shell=True).decode('utf-8')


@attr('cli')
class CLITest(TestCase):

    def _expect_cmd(self, cmd, to_be):
        self.assertEqual(sh(cmd), to_be)

    def test_cli_stdin_simple_render(self):
        self._expect_cmd(
            'echo "{{ foo }}" | tpyl -v foo=bar',
            to_be='bar',
        )

    def test_cli_stdin_env_var_render(self):
        self.assertEqual(
            sh('echo "{{ HOME }}" | tpyl').strip(),
            os.path.expanduser('~'),
        )

    def test_cli_stdin_no_env_render(self):
        self.assertEqual(
            sh('echo "{{ HOME }}" | tpyl --no-env').strip(),
            '',
        )

    def test_cli_stdin_loop_render(self):
        self._expect_cmd(
            'echo "{% for x in y.split(\',\') %}{{ x }}{% endfor %}" | tpyl -v y=1,2,3',
            to_be='123',
        )

    def test_cli_file_yaml_ctx_render(self):
        self._expect_cmd(
            'tpyl %s -c %s' % (_file('template.j2'), _file('cli-yaml.txt')),
            to_be=EXPECTED_RENDER,
        )

    def test_cli_file_json_ctx_render(self):
        self._expect_cmd(
            'tpyl %s -c %s' % (_file('template.j2'), _file('cli-json.txt')),
            to_be=EXPECTED_RENDER,
        )

    def test_cli_file_combined_ctx_render(self):
        self._expect_cmd(
            'tpyl %s -c %s -c %s' % (_file('template.j2'),
                                     _file('cli-partial-yaml.txt'),
                                     _file('cli-partial-json.txt')),
            to_be=EXPECTED_RENDER,
        )

    def test_cli_file_vars_ctx_render(self):
        self._expect_cmd(
            'tpyl %s -c %s -v foo=my-val' % (_file('template.j2'),
                                             _file('cli-partial-json.txt')),
            to_be=EXPECTED_RENDER,
        )

    def test_cli_output_render(self):
        path = _file('test-output')
        sh('echo "{{ foo }}" | tpyl -v foo=bar -o %s' % path)
        with open(path, 'r') as f:
            self.assertEqual(f.read(), 'bar')

    def test_cli_custom_filter(self):
        cm = ('echo "{{ foo | my_custom_filter(3) }}" | tpyl -f %s -v foo=bar'
              % _file(FILTERS_NAME))
        self._expect_cmd(
            cm,
            to_be='bar:bar:bar',
        )

FROM python:3.9.2-alpine3.13

WORKDIR /code

RUN pip install --no-cache-dir --upgrade pip
ADD requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt

ADD tpyl /code/tpyl/
ADD cli.py setup.py /code/
RUN pip install --no-cache-dir --disable-pip-version-check -e /code/

ENTRYPOINT ["tpyl"]

#!/usr/bin/env sh

set -eu

COVER=
IPDB=
HTML=
ATTR=
TESTS=

while test ${#} -gt 0
do
  case "${1}" in
    --cover) COVER=1 ;;
    --ipdb) IPDB=1 ;;
    --html) HTML=1 ;;
    --attr)
      ATTR=${2}
      shift
    ;;
    *)
      TESTS="${TESTS} ${1}"
    ;;
  esac
  shift
done

# Clean
rm -rf htmlcov
rm -rf .coverage

# Run checks
flake8 --max-line-length=100 --exclude=.tox .

# Run test
nosetests -sv \
  ${COVER:+ --with-coverage --cover-package tpyl --cover-inclusive} \
  ${HTML:+ --cover-html --cover-html-dir=htmlcov} \
  ${ATTR:+ --attr ${ATTR}} \
  ${IPDB:+ --ipdb} \
  ${TESTS:+ ${TESTS}}

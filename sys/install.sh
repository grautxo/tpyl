#!/usr/bin/env sh

set -ue

PROJECT="tpyl"

DIR=$(dirname $(realpath ${0}))
VIRTUALENV_PATH="${HOME}/.virtualenvs/${PROJECT}"
VENV_BIN="${VIRTUALENV_PATH}/bin"
URL_PIP="https://bootstrap.pypa.io/get-pip.py"

# Create virtualenv
python3 -m venv "${VIRTUALENV_PATH}" --without-pip

# Install pip
curl "${URL_PIP}" | ${VENV_BIN}/python3

# Install pip dependencies
${VENV_BIN}/pip install -r requirements.txt
${VENV_BIN}/pip install -e $(dirname ${DIR})

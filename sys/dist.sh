#!/usr/bin/env sh

set -ue

PROJECT="tpyl"

DIR=$(dirname $(realpath ${0}))
ROOT=$(dirname "${DIR}")
DIST="${ROOT}/dist"

IMG="tpyl:dist"
CONTAINER="tpyl-dist"


# Build docker image
cat << EOF | docker build --tag "${IMG}" --file - "${ROOT}"
FROM python:3.9.2-slim-buster
WORKDIR /tpyl

# Install dependencies
RUN apt update && \
    apt install -Vy gcc binutils patchelf && \
    rm -rf /var/lib/apt/lists && \
    pip install --no-cache-dir --upgrade pip uv && \
    rm -rf /tmp/* && \
    useradd tpyl && \
    chown tpyl: /tpyl
RUN uv pip install --system --no-cache-dir pyinstaller==6.9.0 pyinstaller-hooks-contrib==2024.7 && \
    uv pip install --system --no-cache-dir SCons==4.8.0 && \
    uv pip install --system --no-cache-dir staticx==0.14.1 && \
    rm -rf /tmp/*
ADD tpyl/ /tpyl/tpyl/
ADD ["cli.py", "requirements.txt", "setup.cfg", "setup.py", "/tpyl/"]
RUN uv pip install --system --no-cache-dir . && \
    rm -rf /tmp/*

# Build binary
USER tpyl
RUN cd /tmp && \
    mkdir -p /tpyl/dist && \
    python -m PyInstaller \
        --name 'tpyl' \
        --noupx \
        --onefile \
        /usr/local/bin/tpyl \
    && \
    mv dist/tpyl /tpyl/dist/tpyl && \
    rm -rf /tmp/*

# The following commands should generate a statically linked binary,
# but it segfaults :)
#    && \
#    staticx \
#        --strip \
#        dist/tpyl dist/tpyl.sx \
#    && \
#    mv dist/tpyl.sx /tpyl/dist/tpyl && \
#    rm dist/tpyl && \
#    rm -rf /tmp/*
EOF

# Create dist binary
mkdir -p "${DIST}"
docker rm -f "${CONTAINER}" || true
docker create --name "${CONTAINER}" --user "$(id -u):$(id -g)" "${IMG}"
docker cp "${CONTAINER}:/tpyl/dist" "${ROOT}"
docker rm "${CONTAINER}"

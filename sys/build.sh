#!/usr/bin/env sh

set -ue

PROJECT="tpyl"

DIR=$(dirname $(realpath ${0}))
ROOT=$(dirname "${DIR}")

# Go to root dir
cd "${ROOT}"

# Create dist
python setup.py sdist

# Create wheel
python setup.py bdist_wheel --universal
